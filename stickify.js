/* 
 * Script development by RedRbrt (made in México), MIT license
 */

$.fn.stickify = function (options) {
  var settings = $.extend({
    zIndex: 1000,
    marginTop: 0,
    smartHide: false, // NOTE: for enable smartHide transition for "top" property must be 0s duration
    alwaysSticky: false,
    useWrapper: true, // off: sticky where item is found, on: sticky moving item inside a wrapper nexto to 'body' selector
    container: window, // opcinally define scroll container (parallax translateZ(-1px) technique require this)
    alreadyFixed: false, // set true for custom elements with already position: fixed and you like to set smarHide
    showGhost: false,
    beforeScroll: function () { },
    beforeSticky: function () { },
    onSticky: function () { },
    onUnSticky: function () { },
    onSwitch: function () { }
  }, options);
  var PREFIX = 'stickify';
  var _this = this;

  this.set = function (setting, value) {
    if (typeof setting == 'string') {
      settings[setting] = value;
    }
    if ($.isPlainObject(setting)) {
      $.extend(settings, setting);
    }
  };

  var generateId = function () {
    var num = Math.floor(Math.random() * 1000) + 1;
    return $('[data-clon="' + PREFIX + '-' + num + '"]').length ? generateId() : PREFIX + '-' + num;
  };

  var durationToMiliseconds = function (duration) {
    return (typeof duration == 'string' && duration.indexOf('s') > -1) ? parseFloat(duration) * 1000 : parseFloat(duration);
  };

  var _onScroll = function (e, scrolling, direction, isHuman) {
    var result = settings.beforeScroll();
    if ((!result && result !== undefined)) {
      return false; // cancel by user
    }
//    var inst = e.data.instance;
    var $e = $(e.data.element);
    if (!settings.alreadyFixed) {
      if (!$e.attr('data-clon')) {
        $e.attr('data-clon', generateId());
      }
      var $clon = $('.clon-' + $e.attr('data-clon'));
      var pos = $clon.length ? $clon.offset() : $e.offset(), stickItNow;
      if (typeof settings.container == 'string') { // for $('.custom-container') scroll
        stickItNow = pos.top - settings.marginTop <= 0;
      } else { // for $(document) scroll
        stickItNow = scrolling > (pos.top - settings.marginTop); // scrolling > to inital offset.top of element
      }
      if (settings.alwaysSticky || stickItNow) {
        _stickOn($e, scrolling, pos, isHuman);
      } else {
        _stickOff($e, scrolling, pos);
      }
    }
    if (settings.smartHide) {
      _smartHide($e, scrolling, direction);
    }
  };

  var _stickOn = function ($e, scrolling, pos, isHuman) {
    if (!$e.hasClass(PREFIX)) {
      settings.beforeSticky.call(_this);
      if ($e.attr('style')) {
        $e.attr('data-style', $e.attr('style'));
      }
      $e.width($e.width());
      // cloning item
//      var $clon = $e.css({ transition: 'none', '-moz-transition': 'none' }).clone();
      var $clon = $e.clone();
      $clon.height($e.height());
      $clon.width($e.width());
      $clon.removeClass(PREFIX).addClass('clon-' + PREFIX + ' clon-' + $e.attr('data-clon')).html('');
      $clon.removeAttr($.map($clon.data(), function (v, k) {
        return 'data-' + k;
      }).join(' '));
      $e.after($clon.css(settings.showGhost ? { background: 'rgba(255,0,0,.3)' } : { visibility: 'hidden' }));
//      $e.after($clon.css({ visibility: 'hidden1', background: 'rgba(255,0,0,.5)' }));
      $e.addClass(PREFIX);
      // moving item to fixed position
      if (settings.useWrapper) {
        var $wrapper = $('.wrapper-' + PREFIX);
        if (!$wrapper.length) {
          $wrapper = $('<div class="wrapper-' + PREFIX + '"></div>').css({
            transition: 'none', '-moz-transition': 'none',
            position: 'fixed', zIndex: settings.zIndex, top: 0
          }).appendTo('body');
        }
        $e.appendTo($wrapper);
      }
      var realLeft = parseInt($e.css('margin-left'));
      var realTop = parseInt($e.css('margin-top')) - settings.marginTop;
      var resultTop = 0 - realTop;
      $e.css({ position: 'fixed', zIndex: $e.css('zIndex') || settings.zIndex, left: pos.left - realLeft, top: resultTop });

      settings.onSticky.call(_this);
      settings.onSwitch.call(_this, true);
    }
  };

  var _stickOff = function ($e, scrolling, pos) {
    if ($e.hasClass(PREFIX)) {
      var $clon = $('.clon-' + $e.attr('data-clon') + ':first');
      if (settings.useWrapper) {
        $e.insertAfter($clon);
      }
      $clon.remove();
      $e.css('zIndex', '').css('position', '').css('height', '').css('width', '').css('left', '').css('top', '').removeClass(PREFIX);
      if ($e.attr('data-style')) {
        $e.attr('style', $e.attr('data-style')).removeAttr('data-style');
      }
      settings.onUnSticky.call(_this);
      settings.onSwitch.call(_this, false);
    }
  };


  var _smartHide = function ($e, scrolling, direction) {
    if ($e.hasClass(PREFIX) || settings.alreadyFixed) {
      setTimeout(function () {
        if (direction == 'up') {
          if ($e.hasClass('smart-hide')) {
            $e.removeClass('smart-hide');
          }
        } else {
          if (!$e.hasClass('smart-hide')) {
            $e.addClass('smart-hide');
          }
        }
      }, 10);
    }
  };

  var lastScrollTop = 0, wheeling, $document;
  var unifyScroll = function (e) {
    var type = e ? e.type : null;
    var scrollTop = $(settings.container).scrollTop();
    var direction = (scrollTop > lastScrollTop) ? 'down' : 'up';
    if (!$document) {
      if (typeof settings.container == 'string') {
        var maxHeight = 0;
        $(settings.container).children().each(function () {
          var itemHeight = $(this).outerHeight(true);
          if (itemHeight > maxHeight) {
            maxHeight = itemHeight;
            $document = $(this);
          }
        });
      } else {
        $document = $(document);
      }
    }
//    console.log('windowH: %o, documentH: %o', $(window).height(), $document.outerHeight(true));
//    return true;
    if (type == 'mousewheel') { // NOTE: mousewheel event is fired before scrollTop refresh, not after      
      direction = e.originalEvent.deltaY > 0 ? 'down' : 'up';
      var alreadyOnTop = direction == 'up' && scrollTop == 0;
      var alreadyOnBottom = direction == 'down' && scrollTop + $(window).height() == $document.outerHeight(true);
      if (alreadyOnTop || alreadyOnBottom) {
        return false;
      }
      var scrollTopAfterWheel = scrollTop + e.originalEvent.deltaY;
      scrollTop = scrollTopAfterWheel > 0 ? scrollTopAfterWheel : 0; // this because wheel event is triggered before scrollTop change
      if (direction == 'down' && scrollTop + $(window).height() > $document.outerHeight(true)) { // detect scroll max to reach bottom
        var extra = scrollTop + $(window).height() - $document.outerHeight(true);
        scrollTop -= extra; // fix wheel extra scroll to match window scroller available
      }
      wheeling = { scrollTop: scrollTop, direction: direction };
    }
    if (type != 'mousewheel' || settings.smartHide) {
      var reachedUp = wheeling && wheeling.direction == 'up' && scrollTop <= wheeling.scrollTop;
      var reachedDown = wheeling && wheeling.direction == 'down' && scrollTop >= wheeling.scrollTop;
      if (reachedUp || reachedDown) {
        direction = wheeling.direction;
        wheeling = null;
      }
      if (!wheeling || settings.smartHide) {
        _this.each(function () {
          $(this).trigger('scrolling', [scrollTop, direction, e.originalEvent ? false : true]);
        });
        lastScrollTop = scrollTop;
      }
    }
  };

  this.each(function () {
    $(this).on('scrolling', { instance: _this, element: $(this) }, _onScroll);
  });
  $(settings.container).on('scroll.' + PREFIX, unifyScroll);
  $(settings.container).on('touchmove.' + PREFIX, unifyScroll); // this event causes issues on mobile rendering
  $(settings.container).on('mousewheel.' + PREFIX, unifyScroll);
  if (!$(settings.container).scrollTop()) {
    //$(settings.container).trigger('scroll.' + PREFIX); // autostart plugin when scroll is on top
  }
  return this;
};
